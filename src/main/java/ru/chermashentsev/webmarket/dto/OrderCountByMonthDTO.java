package ru.chermashentsev.webmarket.dto;

public class OrderCountByMonthDTO {
    private String month;
    private long orderCount;


    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(long orderCount) {
        this.orderCount = orderCount;
    }
}
