package ru.chermashentsev.webmarket.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.chermashentsev.webmarket.entities.Client;
import ru.chermashentsev.webmarket.repos.ClientRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClientService {
    private final ClientRepository clientRepository;

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public Optional<Client> findById(Long id) {
        return clientRepository.findById(id);
    }

    public Optional<Client> findBySurnameAndName(String surname, String name) {
        return clientRepository.findBySurnameAndName(surname, name);
    }

    public void save(Client client) {
        clientRepository.save(client);
    }
}
