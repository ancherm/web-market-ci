create table clients (
                         id SERIAL primary key,
                         surname varchar(255),
                         name varchar(255),
                         phone varchar(255),
                         email varchar(255)
);

create table orders (
                        id SERIAL primary key,
                        client_id integer references clients(id),
                        date_time timestamp
);

create table products (
                          id SERIAL primary key,
                          name varchar(255),
                          price numeric(10, 2),
                          description TEXT,
                          quantity integer,
                          order_id integer references orders(id)
);

insert into products(name, description, price, quantity) values ('Негаз', 'Вода', 60, 100);
insert into products(name, description, price, quantity) values ('Razer', 'Микрофон', 2500, 23);
insert into products(name, description, price, quantity) values ('Механическая', 'Клавиатура', 3000, 210);
insert into products(name, description, price, quantity) values ('Samsung', 'Телефон', 33000, 17);
insert into products(name, description, price, quantity) values ('Проводные', 'Наушники', 1000, 254);