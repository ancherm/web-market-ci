package ru.chermashentsev.webmarket.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.chermashentsev.webmarket.entities.Order;
import ru.chermashentsev.webmarket.repos.OrderRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;


    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    public void save(Order order) {
        orderRepository.save(order);
    }

}
