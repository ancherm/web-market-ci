package ru.chermashentsev.webmarket.controllers.admin;

import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.chermashentsev.webmarket.entities.Order;
import ru.chermashentsev.webmarket.entities.Product;
import ru.chermashentsev.webmarket.services.ClientService;
import ru.chermashentsev.webmarket.services.OrderService;
import ru.chermashentsev.webmarket.services.ProductService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@AllArgsConstructor
public class AdminController {

    private final ProductService productService;
    private final OrderService orderService;
    private final ClientService clientService;


    @GetMapping("/admin/login")
    public String adminLogin() {
        return "admin/login";
    }

    @PostMapping("/admin/login")
    public String adminLogin(@RequestParam String username,
                             @RequestParam String password) {

        if (username.equals("admin") && password.equals("123456")) {
            return "redirect:/admin/catalog";
        }

        return "admin/login";

    }

    @GetMapping("/admin/logout")
    public String adminLogout() {
        return "redirect:/";
    }

    @GetMapping("/admin/catalog")
    public String catalog(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "admin/catalog";
    }

    @GetMapping("/admin/create-product")
    public String createProduct(Model model) {
        model.addAttribute("product", new Product());
        return "admin/create-product";
    }

    @PostMapping("/admin/create-product")
    public String createProduct(@ModelAttribute Product product) {
        productService.save(product);
        return "redirect:/admin/catalog";
    }

    @GetMapping("/admin/edit")
    public String editProduct(@RequestParam Long id, Model model) {
        Product product = productService.getProductById(id).orElse(null);
        model.addAttribute("product", product);

        return "admin/edit";
    }

    @PostMapping("/admin/edit")
    public String editProduct(Product product) {
        productService.save(product);

        return "redirect:/admin/catalog";
    }

    @GetMapping("/admin/delete/{id}")
    public String deleteProduct(@PathVariable Long id) {
        productService.delete(id);

        return "redirect:/admin/catalog";
    }

    @GetMapping("/admin/orders")
    public String orders(Model model) {
        model.addAttribute("orders", orderService.findAll());
        model.addAttribute("clients", clientService.findAll());
        return "admin/orders";
    }

}
