package ru.chermashentsev.webmarket.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.chermashentsev.webmarket.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
