package ru.chermashentsev.webmarket.controllers;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class IndexController {

    @SneakyThrows
    @GetMapping("/")
    public String index() {
        return "index";
    }
}
