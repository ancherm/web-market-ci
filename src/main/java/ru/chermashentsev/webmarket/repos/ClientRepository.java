package ru.chermashentsev.webmarket.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.chermashentsev.webmarket.entities.Client;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findBySurnameAndName(String Surname, String name);
}
