package ru.chermashentsev.webmarket.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.chermashentsev.webmarket.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
