FROM maven:3.8.3-openjdk-17-slim AS builder

WORKDIR /app

COPY pom.xml .
COPY src ./src

RUN mvn clean package -DskipTests

FROM eclipse-temurin:17-jdk-alpine

WORKDIR /app

COPY --from=builder /app/target/web-market-0.0.1-SNAPSHOT.jar /app/web-market.jar

COPY META-INF/MANIFEST.MF /app/META-INF/MANIFEST.MF

ENTRYPOINT ["java", "-jar", "/app/web-market.jar"]

EXPOSE 8080
