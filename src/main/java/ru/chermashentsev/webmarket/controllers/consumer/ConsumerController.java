package ru.chermashentsev.webmarket.controllers.consumer;

import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.chermashentsev.webmarket.entities.Client;
import ru.chermashentsev.webmarket.entities.Order;
import ru.chermashentsev.webmarket.entities.Product;
import ru.chermashentsev.webmarket.services.ClientService;
import ru.chermashentsev.webmarket.services.OrderService;
import ru.chermashentsev.webmarket.services.ProductService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
public class ConsumerController {
    private final ClientService clientService;
    private final ProductService productService;
    private final OrderService orderService;


    @GetMapping("/consumer/logout")
    public String consumerLogout() {
        return "redirect:/";
    }

    @GetMapping("/consumer/catalog")
    public String catalog(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "consumer/catalog";
    }

    @GetMapping("/consumer/ordering")
    public String ordering(Model model, @RequestParam Long productId) {
        Product product = productService.getProductById(productId).orElse(null);

        if (product == null || product.getQuantity() <= 0) {
            return "redirect:/consumer/catalog";
        }

        product.setQuantity(product.getQuantity() - 1);
        productService.save(product);

        model.addAttribute("product", product);
        model.addAttribute("client", new Client());
        return "consumer/ordering";
    }

    @PostMapping("/consumer/ordering")
    public String ordering(Client client, @RequestParam Long productId) {
        Optional<Client> existingClient = clientService.findBySurnameAndName(client.getSurname(), client.getName());

        if (existingClient.isPresent()) {
            client = existingClient.get();
        } else {
            clientService.save(client);
        }

        Product product = productService.getProductById(productId).orElse(null);
        if (product == null) {
            return "redirect:/consumer/catalog";
        }

        Order order = Order.builder()
                .client(client)
                .productList(List.of(product))
                .dateTime(LocalDateTime.now())
                .build();

        orderService.save(order);

        return "redirect:/consumer/successful-order";
    }

    @GetMapping("/consumer/successful-order")
    public String ordered() {
        return "consumer/successful-order";
    }
}
